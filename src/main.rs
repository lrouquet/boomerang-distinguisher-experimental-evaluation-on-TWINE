use std::fs::File;
use std::io;
use std::io::{BufReader};
use std::path::PathBuf;
use clap::Parser;
use rand::{RngCore, SeedableRng};
use rand_xoshiro::{Xoshiro256StarStar};
use serde::{Serialize, Deserialize};
use rayon::prelude::*;

const BR: usize = 16;
const BR_HALF: usize = 8;
const MAX_NR: usize = 36;

const S: [u8; 16] = [
    0x0c, 0x00, 0x0f, 0x0a,
    0x02, 0x0b, 0x09, 0x05,
    0x08, 0x03, 0x0d, 0x07,
    0x01, 0x0e, 0x06, 0x04
];
const PI: [usize; BR] = [
    0x05, 0x00, 0x01, 0x04,
    0x07, 0x0c, 0x03, 0x08,
    0x0d, 0x06, 0x09, 0x02,
    0x0f, 0x0a, 0x0b, 0x0e
];
const PI_INV: [usize; BR] = [
    0x01, 0x02, 0x0b, 0x06,
    0x03, 0x00, 0x09, 0x04,
    0x07, 0x0a, 0x0d, 0x0e,
    0x05, 0x08, 0x0f, 0x0c
];
const CON: [u8; 35] = [
    0x01, 0x02, 0x04, 0x08,
    0x10, 0x20, 0x03, 0x06,
    0x0c, 0x18, 0x30, 0x23,
    0x05, 0x0a, 0x14, 0x28,
    0x13, 0x26, 0x0f, 0x1e,
    0x3c, 0x3b, 0x35, 0x29,
    0x11, 0x22, 0x07, 0x0e,
    0x1c, 0x38, 0x33, 0x25,
    0x09, 0x12, 0x24
];

fn one_round(x: &mut [u8; BR], k: &[u8; BR_HALF], perm: &[usize; BR], tmp: &mut [u8; BR]) {
    for i in 0..BR_HALF {
        x[2 * i + 1] = S[(x[2 * i] ^ k[i]) as usize] ^ x[2 * i + 1] & 0x0F;
    }

    for i in 0..BR {
        tmp[perm[i]] = x[i];
    }
    std::mem::swap(x, tmp);
}

fn permutation(x: &mut [u8; BR], perm: &[usize; BR], tmp: &mut [u8; BR]) {
    for i in 0..BR {
        tmp[perm[i]] = x[i];
    }
    std::mem::swap(x, tmp);
}

fn cipher<'l>(nr: usize, x: &'l mut [u8; BR], sub_key: &[[u8; BR_HALF]; MAX_NR]) -> &'l mut [u8; BR] {
    let mut tmp = [0; BR];
    for i in 0..nr - 1 {
        one_round(x, &sub_key[i], &PI, &mut tmp);
    }
    for i in 0..BR_HALF {
        x[2 * i + 1] = S[(x[2 * i] ^ sub_key[nr - 1][i]) as usize] ^ x[2 * i + 1] & 0x0F;
    }
    x
}

fn decipher<'l>(nr: usize, x: &'l mut [u8; BR], sub_key: &[[u8; BR_HALF]; MAX_NR]) -> &'l mut [u8; BR] {
    let mut tmp = [0; BR];
    for i in (1..=(nr - 1)).rev() {
        one_round(x, &sub_key[i], &PI_INV, &mut tmp);
    }
    for i in 0..BR_HALF {
        x[2 * i + 1] = S[(x[2 * i] ^ sub_key[0][i]) as usize] ^ x[2 * i + 1] & 0x0F;
    }
    x
}

fn key_schedule_80bits(nr: usize, key: &Vec<u16>, output: &mut [[u8; BR_HALF]; MAX_NR]) {
    let mut key_r = [0u8; 40];
    let mut temp: u8;
    let mut temp1: u8;
    let mut temp2: u8;
    let mut temp3: u8;

    for i in 0..20 {
        key_r[i] = (key[(i / 4)] >> (4 * (i & 0x03))) as u8 & 0x0F;
    }

    for i in 0..nr - 1 {
        output[i][0] = key_r[1] as u8;
        output[i][1] = key_r[3] as u8;
        output[i][2] = key_r[4] as u8;
        output[i][3] = key_r[6] as u8;
        output[i][4] = key_r[13] as u8;
        output[i][5] = key_r[14] as u8;
        output[i][6] = key_r[15] as u8;
        output[i][7] = key_r[16] as u8;

        key_r[1] = key_r[1] ^ S[key_r[0] as usize];
        key_r[4] = key_r[4] ^ S[key_r[16] as usize];
        key_r[7] = key_r[7] ^ (CON[i] >> 3);
        key_r[19] = key_r[19] ^ (CON[i] & 0x07);

        temp = key_r[0];
        key_r[0] = key_r[1];
        key_r[1] = key_r[2];
        key_r[2] = key_r[3];
        key_r[3] = temp;

        temp = key_r[0];
        temp1 = key_r[1];
        temp2 = key_r[2];
        temp3 = key_r[3];

        key_r[0] = key_r[4];
        key_r[1] = key_r[5];
        key_r[2] = key_r[6];
        key_r[3] = key_r[7];

        key_r[4] = key_r[8];
        key_r[5] = key_r[9];
        key_r[6] = key_r[10];
        key_r[7] = key_r[11];

        key_r[8] = key_r[12];
        key_r[9] = key_r[13];
        key_r[10] = key_r[14];
        key_r[11] = key_r[15];

        key_r[12] = key_r[16];
        key_r[13] = key_r[17];
        key_r[14] = key_r[18];
        key_r[15] = key_r[19];

        key_r[16] = temp;
        key_r[17] = temp1;
        key_r[18] = temp2;
        key_r[19] = temp3;
    }

    output[35][0] = key_r[1] as u8;
    output[35][1] = key_r[3] as u8;
    output[35][2] = key_r[4] as u8;
    output[35][3] = key_r[6] as u8;
    output[35][4] = key_r[13] as u8;
    output[35][5] = key_r[14] as u8;
    output[35][6] = key_r[15] as u8;
    output[35][7] = key_r[16] as u8;
}

fn key_schedule_128bits(nr: usize, key: &Vec<u16>, output: &mut [[u8; BR_HALF]; MAX_NR]) {
    let mut key_r = [0u8; 64];
    let mut temp: u8;
    let mut temp1: u8;
    let mut temp2: u8;
    let mut temp3: u8;

    for i in 0..32 {
        key_r[i] = (key[(i / 4)] >> (4 * (i & 0x03))) as u8 & 0x0F;
    }

    for i in 0..nr - 1 {
        output[i][0] = key_r[2] as u8;
        output[i][1] = key_r[3] as u8;
        output[i][2] = key_r[12] as u8;
        output[i][3] = key_r[15] as u8;
        output[i][4] = key_r[17] as u8;
        output[i][5] = key_r[18] as u8;
        output[i][6] = key_r[28] as u8;
        output[i][7] = key_r[31] as u8;

        key_r[1] = key_r[1] ^ S[key_r[0] as usize];
        key_r[4] = key_r[4] ^ S[key_r[16] as usize];
        key_r[23] = key_r[23] ^ S[key_r[30] as usize];

        key_r[7] = key_r[7] ^ (CON[i] >> 3);
        key_r[19] = key_r[19] ^ (CON[i] & 0x07);

        temp = key_r[0];
        key_r[0] = key_r[1];
        key_r[1] = key_r[2];
        key_r[2] = key_r[3];
        key_r[3] = temp;

        temp = key_r[0];
        temp1 = key_r[1];
        temp2 = key_r[2];
        temp3 = key_r[3];

        key_r[0] = key_r[4];
        key_r[1] = key_r[5];
        key_r[2] = key_r[6];
        key_r[3] = key_r[7];

        key_r[4] = key_r[8];
        key_r[5] = key_r[9];
        key_r[6] = key_r[10];
        key_r[7] = key_r[11];

        key_r[8] = key_r[12];
        key_r[9] = key_r[13];
        key_r[10] = key_r[14];
        key_r[11] = key_r[15];

        key_r[12] = key_r[16];
        key_r[13] = key_r[17];
        key_r[14] = key_r[18];
        key_r[15] = key_r[19];

        key_r[16] = key_r[20];
        key_r[17] = key_r[21];
        key_r[18] = key_r[22];
        key_r[19] = key_r[23];

        key_r[20] = key_r[24];
        key_r[21] = key_r[25];
        key_r[22] = key_r[26];
        key_r[23] = key_r[27];

        key_r[24] = key_r[28];
        key_r[25] = key_r[29];
        key_r[26] = key_r[30];
        key_r[27] = key_r[31];

        key_r[28] = temp;
        key_r[29] = temp1;
        key_r[30] = temp2;
        key_r[31] = temp3;
    }

    output[35][0] = key_r[2] as u8;
    output[35][1] = key_r[3] as u8;
    output[35][2] = key_r[12] as u8;
    output[35][3] = key_r[15] as u8;
    output[35][4] = key_r[17] as u8;
    output[35][5] = key_r[18] as u8;
    output[35][6] = key_r[28] as u8;
    output[35][7] = key_r[31] as u8;
}

const _80BITS_KEY: usize = 80;
const _128BITS_KEY: usize = 128;
const NIBBLES_BY_INT: usize = 4;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    #[clap(short, long)]
    distinguisher_path: PathBuf,
    #[clap(short, long)]
    nb_threads: Option<usize>,
}

#[derive(Serialize, Deserialize)]
struct Distinguisher {
    nr: usize,
    key_len: usize,
    input_difference: [Option<u8>; BR],
    output_difference: [Option<u8>; BR],
    log2_nb_keys: usize,
    log2_nb_tries_by_key: usize,
}

fn main() -> io::Result<()> {
    let Cli { distinguisher_path, nb_threads } = Cli::parse();
    if let Some(nb_threads) = nb_threads {
        rayon::ThreadPoolBuilder::new()
            .num_threads(nb_threads)
            .build_global()
            .unwrap();
    }

    let distinguisher_file = File::open(distinguisher_path)?;
    let buf_reader = BufReader::new(distinguisher_file);
    let Distinguisher {
        nr,
        key_len,
        log2_nb_keys,
        log2_nb_tries_by_key,
        input_difference,
        output_difference
    } = serde_json::from_reader(buf_reader)?;

    if key_len != 80 && key_len != 128 {
        panic!("Invalid key_len. Expected is either 80 or 128.");
    }

    let seed = [
        6, 3, 14, 7, 11, 12, 0, 15, 8, 2, 1, 10, 4, 5, 9, 13,
        14, 9, 6, 3, 7, 2, 8, 11, 10, 5, 0, 15, 1, 4, 13, 12
    ];

    let mut rand = Xoshiro256StarStar::from_seed(seed);
    #[allow(non_snake_case, unused_variables, unused_mut)]
        let mut FREE = || {
        let mut tmp = rand.next_u64() as u8 & 0xf;
        while tmp == 0 {
            tmp = rand.next_u64() as u8 & 0xf
        }
        tmp
    };

    // Replace null (aka FREE) variables with a pseudo-random value != 0.
    let input_difference = input_difference.map(|it| it.unwrap_or_else(&mut FREE));
    let mut output_difference = output_difference.map(|it| it.unwrap_or_else(&mut FREE));

    // Apply inverse permutation to the output differences to match the current implementation
    permutation(&mut output_difference, &PI_INV, &mut [0; BR]);

    // nb_keys = 2^{log2_nb_keys}
    let nb_keys = 1usize << log2_nb_keys;
    // nb_tries_by_key = 2^{log2_nb_tries_by_key}
    let nb_tries_by_key = 1usize << log2_nb_tries_by_key;

    let boomerang = SingleKeyBoomerang::new(&input_difference, &output_difference, nr);
    let mut key = vec![0; key_len / NIBBLES_BY_INT];
    let mut nb_returns = 0;
    for key_index in 0..nb_keys {
        for k in 0..key_len / NIBBLES_BY_INT {
            key[k] = (rand.next_u64() ^ k as u64 ^ key_index as u64) as u16;
        }

        let mut expanded_key = [[0; BR_HALF]; MAX_NR];
        if key_len == 80 {
            key_schedule_80bits(MAX_NR, &key, &mut expanded_key);
        } else if key_len == 128 {
            key_schedule_128bits(MAX_NR, &key, &mut expanded_key);
        }

        let message_generator = RandomMessageGenerator(&mut rand);
        let nb_returns_for_key = message_generator
            .take(nb_tries_by_key)
            .par_bridge()
            .map(|mut m| boomerang.is_returning_for(&mut m, &expanded_key))
            .sum::<usize>();
        println!("Boomerang: {}/{}, proba = 2^{{{:.4}}}", nb_returns_for_key, nb_tries_by_key, (nb_returns_for_key as f64).log2() - (nb_tries_by_key as f64).log2());
        nb_returns += nb_returns_for_key;
    }
    println!("Mean: {:.2}/{}, proba = 2^{{{:.4}}}", nb_returns as f64 / nb_keys as f64, nb_tries_by_key, (nb_returns as f64).log2() - ((nb_keys * nb_tries_by_key) as f64).log2());
    Ok(())
}

struct SingleKeyBoomerang<'a> {
    input_difference: &'a [u8; BR],
    output_difference: &'a [u8; BR],
    nr: usize,
}

impl<'a> SingleKeyBoomerang<'a> {
    fn new(
        input_difference: &'a [u8; BR],
        output_difference: &'a [u8; BR],
        nr: usize,
    ) -> SingleKeyBoomerang<'a> {
        SingleKeyBoomerang { input_difference, output_difference, nr }
    }

    fn is_returning_for(&self, m: &mut [u8; BR], key: &[[u8; BR_HALF]; MAX_NR]) -> usize {
        let mut m1 = [0; BR];
        for k in 0..BR {
            m1[k] = m[k] ^ self.input_difference[k];
        }

        cipher(self.nr, m, key);
        cipher(self.nr, &mut m1, key);

        for k in 0..BR {
            m[k] ^= self.output_difference[k];
            m1[k] ^= self.output_difference[k];
        }

        decipher(self.nr, m, key);
        decipher(self.nr, &mut m1, key);

        for k in 0..BR {
            if m[k] ^ m1[k] != self.input_difference[k] {
                return 0;
            }
        }
        1
    }
}

fn to_nibble(b: u64) -> u8 {
    (b as u8) & 0xF
}

struct RandomMessageGenerator<'a, R>(&'a mut R);

impl<'a, R> Iterator for RandomMessageGenerator<'a, R> where R: RngCore {
    type Item = [u8; BR];

    fn next(&mut self) -> Option<Self::Item> {
        // random_message = m0 concatenate m1.
        let mut random_message = [0; BR];
        //let m0 = self.0.next_u64();
        //let m1 = self.0.next_u64();
        for k in 0..BR {
            random_message[k] = to_nibble(self.0.next_u64());
        }
        Some(random_message)
    }
}

#[cfg(test)]
mod tests {
    use crate::{BR_HALF, decipher, cipher, MAX_NR, _80BITS_KEY, _128BITS_KEY, key_schedule_80bits, key_schedule_128bits, NIBBLES_BY_INT};

    #[test]
    fn cipher_test_vector_80bits() {
        let mut plain = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF];
        let mut key = vec![0; _80BITS_KEY / NIBBLES_BY_INT];
        key[0] = 0x1100;
        key[1] = 0x3322;
        key[2] = 0x5544;
        key[3] = 0x7766;
        key[4] = 0x9988;

        let mut sub_key = [[0; BR_HALF]; MAX_NR];
        key_schedule_80bits(MAX_NR, &key, &mut sub_key);
        cipher(MAX_NR, &mut plain, &sub_key);
        assert_eq!(plain, [0x7, 0xC, 0x1, 0xF, 0x0, 0xF, 0x8, 0x0, 0xB, 0x1, 0xD, 0xF, 0x9, 0xC, 0x2, 0x8])
    }

    #[test]
    fn cipher_test_vector_128bits() {
        let mut plain = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF];
        let mut key = vec![0; _128BITS_KEY / NIBBLES_BY_INT];
        key[0] = 0x1100;
        key[1] = 0x3322;
        key[2] = 0x5544;
        key[3] = 0x7766;
        key[4] = 0x9988;
        key[5] = 0xBBAA;
        key[6] = 0xDDCC;
        key[7] = 0xFFEE;

        let mut sub_key = [[0; BR_HALF]; MAX_NR];
        key_schedule_128bits(MAX_NR, &key, &mut sub_key);
        cipher(MAX_NR, &mut plain, &sub_key);
        assert_eq!(plain, [0x9, 0x7, 0x9, 0xF, 0xF, 0x9, 0xB, 0x3, 0x7, 0x9, 0xB, 0x5, 0xA, 0x9, 0xB, 0x8])
    }

    #[test]
    fn decipher_test_vector_80bits() {
        let mut cipher = [0x7, 0xC, 0x1, 0xF, 0x0, 0xF, 0x8, 0x0, 0xB, 0x1, 0xD, 0xF, 0x9, 0xC, 0x2, 0x8];
        let mut key = vec![0; _80BITS_KEY / NIBBLES_BY_INT];
        key[0] = 0x1100;
        key[1] = 0x3322;
        key[2] = 0x5544;
        key[3] = 0x7766;
        key[4] = 0x9988;

        let mut sub_key = [[0; BR_HALF]; MAX_NR];
        key_schedule_80bits(MAX_NR, &key, &mut sub_key);
        decipher(MAX_NR, &mut cipher, &sub_key);
        assert_eq!(cipher, [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF]);
    }

    #[test]
    fn decipher_test_vector_128bits() {
        let mut cipher = [0x9, 0x7, 0x9, 0xF, 0xF, 0x9, 0xB, 0x3, 0x7, 0x9, 0xB, 0x5, 0xA, 0x9, 0xB, 0x8];
        let mut key = vec![0; _128BITS_KEY / NIBBLES_BY_INT];
        key[0] = 0x1100;
        key[1] = 0x3322;
        key[2] = 0x5544;
        key[3] = 0x7766;
        key[4] = 0x9988;
        key[5] = 0xBBAA;
        key[6] = 0xDDCC;
        key[7] = 0xFFEE;

        let mut sub_key = [[0; BR_HALF]; MAX_NR];
        key_schedule_128bits(MAX_NR, &key, &mut sub_key);
        decipher(MAX_NR, &mut cipher, &sub_key);
        assert_eq!(cipher, [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF])
    }
}